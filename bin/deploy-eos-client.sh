#!/bin/bash
#

# eos to copy files to /eos
eos="/usr/bin/eos"
if [ ! -x $eos ]
then
    echo "ERROR: $eos not found"
    exit 1
fi

# Rely on eos to do the copy of files to EOS
$eos cp -r "$CI_OUTPUT_DIR/*" "$EOS_PATH/" 2>&1 >/dev/null
if [ $? -ne 0 ]
then
    echo "ERROR: Failed to copy files from '$CI_OUTPUT_DIR/' to '$EOS_PATH' via eos"
    exit 1
fi
