
This project builds the special images that are used
for TDAQ continuous integration tests. 

  - Two base images based on CentOS7 and Alma 0 rsp., both for
    x86 and aarch64. A user
    can specify these in the image: key and execute
    arbitrary commands on it. The images have all additional
    RPMs that are needed for development installed.

       image: gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-centos7
       image: gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-el9
       image: gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:aarch64-centos7
       image: gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:aarch64-el9


